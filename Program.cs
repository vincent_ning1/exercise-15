﻿using System;

namespace exercise_15
{
    class Program
    {
        static void Main(string[] args)
        {
           //Start the program with Clear();
           Console.Clear();
           Console.WriteLine("Please type in a number as your lucky number");

           var number = int.Parse(Console.ReadLine());
          Console.WriteLine($"The number that u typed inmultiplied 1 is {number*1}");
           Console.WriteLine($"The number that u typed inmultiplied 2 is {number*2}");
            Console.WriteLine($"The number that u typed inmultiplied 3 is {number*3}");
             Console.WriteLine($"The number that u typed inmultiplied 4 is {number*4}");
              Console.WriteLine($"The number that u typed inmultiplied 5 is {number*5}");
               Console.WriteLine($"The number that u typed inmultiplied 6 is {number*6}");
                Console.WriteLine($"The number that u typed inmultiplied 7 is {number*7}");
                 Console.WriteLine($"The number that u typed inmultiplied 8 is {number*8}");
                  Console.WriteLine($"The number that u typed inmultiplied 9 is {number*9}");
                   Console.WriteLine($"The number that u typed inmultiplied 10 is {number*10}");
                    Console.WriteLine($"The number that u typed inmultiplied 11 is {number*11}");
                     Console.WriteLine($"The number that u typed inmultiplied 12 is {number*12}");
           
           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();
        }
    }
}
